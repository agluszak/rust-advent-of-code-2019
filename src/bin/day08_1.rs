use std::io;

fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    let digits = input.trim().chars()
        .map(|x| x.to_digit(10).unwrap() as usize).collect::<Vec<_>>();
    let width = 25;
    let height = 6;

    let counts: Vec<[usize; 3]> = digits.chunks(width * height).map(|layer| {
        let mut count = [0, 0, 0];
        for &d in layer {
            count[d] += 1;
        }
        count
    }).collect();
    let mut fewest_zeros_index = 0;
    let mut fewest_zeros = width * height;
    for (i, count) in counts.iter().enumerate() {
        if count[0] < fewest_zeros {
            fewest_zeros = count[0];
            fewest_zeros_index = i;
        }
    }
    println!("{}", counts[fewest_zeros_index][1] * counts[fewest_zeros_index][2]);
}