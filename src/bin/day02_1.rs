use std::io;

fn run(mut opcodes: Vec<i32>) -> Vec<i32> {
    let mut position = 0;
    loop {
        let opcode = opcodes.get(position).unwrap();
        match opcode {
            1 => {
                let (first_pos, second_pos, dest) = (opcodes[position + 1] as usize,
                                                     opcodes[position + 2] as usize,
                                                     opcodes[position + 3] as usize);
                opcodes[dest] = opcodes[first_pos] + opcodes[second_pos];
                position += 4;
            }
            2 => {
                let (first_pos, second_pos, dest) = (opcodes[position + 1] as usize,
                                                     opcodes[position + 2] as usize,
                                                     opcodes[position + 3] as usize);
                opcodes[dest] = opcodes[first_pos] * opcodes[second_pos];
                position += 4;
            }
            99 => {
                break opcodes;
            }
            _ => {
                panic!("Incorrect value")
            }
        }
    }
}

fn main() -> Result<(), std::io::Error> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    let split = input.trim().split(',');
    let mut opcodes = split.map(|x| x.parse::<i32>().unwrap()).collect::<Vec<_>>();
    opcodes[1] = 12;
    opcodes[2] = 2;
    println!("{:?}", run(opcodes));

    Ok(())
}

#[test]
fn test() {
    assert_eq!(run(vec![1, 1, 1, 4, 99, 5, 6, 0, 99]), vec![30, 1, 1, 4, 2, 5, 6, 0, 99]);
    assert_eq!(run(vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]), vec![3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]);
}
