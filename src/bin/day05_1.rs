use std::io;

fn mode(opcode: i32, nth_param: usize) -> i32 {
    let mut rest = opcode / 100;
    for _ in 1..nth_param {
        rest /= 10;
    }
    rest % 10
}

fn value(opcodes: &Vec<i32>, position: usize, nth_param: usize) -> i32 {
    let opcode = opcodes[position];
    match mode(opcode, nth_param) {
        0 => opcodes[opcodes[position + nth_param] as usize],
        1 => opcodes[position + nth_param],
        _ => panic!("Incorrect mode")
    }
}

fn read_input() -> i32 {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    input.trim().parse::<i32>().unwrap()
}

fn run(mut opcodes: Vec<i32>) -> Vec<i32> {
    let mut position = 0;
    loop {
        let opcode = opcodes[position];
        let operation = opcode % 100;
        match operation {
            1 => {
                let dest = opcodes[position + 3] as usize;
                opcodes[dest] = value(&opcodes, position, 1) + value(&opcodes, position, 2);
                position += 4;
            }
            2 => {
                let dest = opcodes[position + 3] as usize;
                opcodes[dest] = value(&opcodes, position, 1) * value(&opcodes, position, 2);
                position += 4;
            }
            3 => {
                let dest = opcodes[position + 1] as usize;
                opcodes[dest] = read_input();
                position += 2;
            }
            4 => {
                println!("{}", value(&opcodes, position, 1));
                position += 2;
            }
            99 => {
                break opcodes;
            }
            _ => {
                panic!("Incorrect value")
            }
        }
    }
}

fn main() -> Result<(), std::io::Error> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    let split = input.trim().split(',');
    let original_opcodes = split.map(|x| x.parse::<i32>().unwrap()).collect::<Vec<_>>();
    run(original_opcodes);
    Ok(())
}