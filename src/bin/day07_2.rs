use std::cmp::max;
use std::io;

use itertools::Itertools;

fn mode(opcode: i32, nth_param: usize) -> i32 {
    let mut rest = opcode / 100;
    for _ in 1..nth_param {
        rest /= 10;
    }
    rest % 10
}

fn value(opcodes: &Vec<i32>, position: usize, nth_param: usize) -> i32 {
    let opcode = opcodes[position];
    match mode(opcode, nth_param) {
        0 => opcodes[opcodes[position + nth_param] as usize],
        1 => opcodes[position + nth_param],
        _ => panic!("Incorrect mode")
    }
}

fn set_value(opcodes: &mut Vec<i32>, position: usize, nth_param: usize, value: i32) {
    let opcode = opcodes[position];
    match mode(opcode, nth_param) {
        0 => {
            let dest = opcodes[position + nth_param] as usize;
            opcodes[dest] = value
        }
        1 => opcodes[position + nth_param] = value,
        _ => panic!("Incorrect mode")
    }
}


fn run(mut opcodes: &mut Vec<i32>, position: &mut usize, input: &mut Vec<i32>) -> Option<i32> {
    loop {
        let opcode = opcodes[*position];
        let operation = opcode % 100;
        match operation {
            1 => {
                let dest = opcodes[*position + 3] as usize;
                opcodes[dest] = value(&opcodes, *position, 1) + value(&opcodes, *position, 2);
                *position += 4;
            }
            2 => {
                let dest = opcodes[*position + 3] as usize;
                opcodes[dest] = value(&opcodes, *position, 1) * value(&opcodes, *position, 2);
                *position += 4;
            }
            3 => {
                let dest = opcodes[*position + 1] as usize;
                opcodes[dest] = input.remove(0);
                *position += 2;
            }
            4 => {
                let ret = value(&opcodes, *position, 1);
                *position += 2;
                break Some(ret);
            }
            5 => {
                let check = value(&opcodes, *position, 1);
                if check != 0 {
                    *position = value(&opcodes, *position, 2) as usize;
                } else {
                    *position += 3;
                }
            }
            6 => {
                let check = value(&opcodes, *position, 1);
                if check == 0 {
                    *position = value(&opcodes, *position, 2) as usize;
                } else {
                    *position += 3;
                }
            }
            7 => {
                let first = value(&opcodes, *position, 1);
                let second = value(&opcodes, *position, 2);
                if first < second {
                    set_value(&mut opcodes, *position, 3, 1);
                } else {
                    set_value(&mut opcodes, *position, 3, 0);
                }
                *position += 4;
            }
            8 => {
                let first = value(&opcodes, *position, 1);
                let second = value(&opcodes, *position, 2);
                if first == second {
                    set_value(&mut opcodes, *position, 3, 1);
                } else {
                    set_value(&mut opcodes, *position, 3, 0);
                }
                *position += 4;
            }
            99 => break None,
            v => {
                panic!("Incorrect value {}", v)
            }
        }
    }
}

fn main() -> Result<(), std::io::Error> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    let split = input.trim().split(',');
    let original_opcodes = split.map(|x| x.parse::<i32>().unwrap()).collect::<Vec<_>>();

    const AMPS: usize = 5;

    let mut best = 0;
    let possible_phases = 5..=9;
    for phases in possible_phases.permutations(AMPS) {
        let mut i = 0;
        let mut signal = 0;
        let mut programs = vec![original_opcodes.clone(); AMPS];
        let mut positions = vec![0; AMPS];
        let mut inputs: Vec<Vec<i32>> = phases.clone().iter().map(|&n| { vec![n] }).collect();
        inputs[0].push(0);
        loop {
            if let Some(new_signal) = run(&mut programs[i], &mut positions[i], &mut inputs[i]) {
                signal = new_signal;
                i += 1;
                i = i % AMPS;
                inputs[i].push(signal)
            } else {
                best = max(best, signal);
                break;
            }
        }
    }

    println!("{}", best);
    Ok(())
}

