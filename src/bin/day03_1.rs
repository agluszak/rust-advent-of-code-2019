use std::collections::HashSet;
use std::hash::Hash;
use std::io::{Error, stdin};
use std::io;
use std::iter::FromIterator;

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
struct Coord(i32, i32);

enum Dir { L, U, R, D }

struct Twist(Dir, i32);

fn parse_twist(s: String) -> Twist {
    let dir = match s.chars().next().unwrap() {
        'L' => Dir::L,
        'U' => Dir::U,
        'R' => Dir::R,
        'D' => Dir::D,
        _ => panic!("Wrong dir")
    };
    let len = s[1..].parse::<i32>().unwrap();
    Twist(dir, len)
}

fn append_coords(list: &mut Vec<Coord>, twist: &Twist) {
    let mut last = list[list.len() - 1];
    for i in 0..twist.1 {
        match twist.0 {
            Dir::L => {
                last = Coord(last.0 - 1, last.1)
            }
            Dir::U => {
                last = Coord(last.0, last.1 + 1)
            }
            Dir::R => {
                last = Coord(last.0 + 1, last.1)
            }
            Dir::D => {
                last = Coord(last.0, last.1 - 1)
            }
        }
        list.push(last);
    }
}

fn read() -> String {
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    input
}

fn read_path(s: String) -> HashSet<Coord> {
    let raw_path: Vec<Twist> = s.trim().split(',').map(|s| parse_twist(s.to_owned())).collect::<Vec<_>>();
    let mut vec = vec![Coord(0, 0)];
    raw_path.iter().for_each(|t| append_coords(&mut vec, t));
    let mut result = HashSet::from_iter(vec.iter().cloned());
    result.remove(&Coord(0, 0));
    result
}

fn manhattan_distance(c1: Coord, c2: Coord) -> i32 {
    (c1.0 - c2.0).abs() + (c1.1 - c2.1).abs()
}

fn min_distance(s1: HashSet<Coord>, s2: HashSet<Coord>) -> i32 {
    s1.intersection(&s2).map(|c| manhattan_distance(*c, Coord(0, 0))).min().unwrap()
}

fn main() {
    let first_path: HashSet<Coord> = read_path(read());
    let second_path: HashSet<Coord> = read_path(read());
    println!("{}", min_distance(first_path, second_path));
}

#[test]
fn test() {
    assert_eq!(min_distance(read_path("R75,D30,R83,U83,L12,D49,R71,U7,L72".to_string()), read_path("U62,R66,U55,R34,D71,R55,D58,R83".to_string())), 159);
    assert_eq!(min_distance(read_path("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51".to_string()), read_path("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7".to_string())), 135);
}
