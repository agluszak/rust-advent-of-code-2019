use std::io;
use itertools::Itertools;

fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    let digits = input.trim().chars()
        .map(|x| x.to_digit(10).unwrap() as usize).collect::<Vec<_>>();
    const WIDTH: usize = 25;
    const HEIGHT: usize = 6;
    let layers: Vec<Vec<usize>> = digits.chunks(WIDTH * HEIGHT).map(|v| v.to_vec()).collect();
    let mut final_image = [2; WIDTH * HEIGHT];
    for p in 0..WIDTH * HEIGHT {
        for layer in &layers {
            if final_image[p] == 2 {
                final_image[p] = layer[p];
            }
        }
    }

    let out = final_image
        .iter()
        .map(|&i| if i == 0 {" "} else {"#"})
        .collect::<Vec<&str>>()
        .chunks(WIDTH)
        .map(|s|s.join(""))
        .join("\n");
    println!("{}", out);
}