use std::collections::{HashMap, VecDeque};
use std::io;

use regex::Regex;

fn main() {
    let initial = "COM".to_string();
    let regex = Regex::new(r"(.+)\)(.+)").unwrap();
    let mut input = String::new();
    let mut children: HashMap<String, Vec<String>> = HashMap::new();
    let mut parents: HashMap<String, String> = HashMap::new();
    let mut size: HashMap<String, i32> = HashMap::new();
    loop {
        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        if let Some(matches) = regex.captures(input.trim()) {
            let parent = matches[1].to_string();
            let child = matches[2].to_string();
            children.entry(parent.clone())
                .and_modify(|v| v.push(child.clone()))
                .or_insert(vec![child.clone()]);
            parents.insert(child, parent);
        } else {
            break;
        }
    }

    let mut queue = VecDeque::new();
    queue.push_back(initial.clone());
    size.insert(initial.clone(), 0);
    while let Some(v) = queue.pop_back() {
        let &v_size = size.get(&v).unwrap();
        for child in children.get(&v).unwrap_or(&Vec::new()) {
            size.insert(child.clone(), v_size + 1);
            queue.push_back(child.clone());
        }
    }
    let mut sum = 0;
    for &v in size.values() {
        sum += v;
    }
    println!("{}", sum);
}