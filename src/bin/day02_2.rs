use std::io;

fn run(mut opcodes: Vec<i32>) -> Vec<i32> {
    let mut position = 0;
    loop {
        let opcode = opcodes.get(position).unwrap();
        match opcode {
            1 => {
                let (first_pos, second_pos, dest) = (opcodes[position + 1] as usize,
                                                     opcodes[position + 2] as usize,
                                                     opcodes[position + 3] as usize);
                opcodes[dest] = opcodes[first_pos] + opcodes[second_pos];
                position += 4;
            }
            2 => {
                let (first_pos, second_pos, dest) = (opcodes[position + 1] as usize,
                                                     opcodes[position + 2] as usize,
                                                     opcodes[position + 3] as usize);
                opcodes[dest] = opcodes[first_pos] * opcodes[second_pos];
                position += 4;
            }
            99 => {
                break opcodes;
            }
            _ => {
                panic!("Incorrect value")
            }
        }
    }
}

fn main() -> Result<(), std::io::Error> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    let split = input.trim().split(',');
    let original_opcodes = split.map(|x| x.parse::<i32>().unwrap()).collect::<Vec<_>>();


    for noun in 0..100 {
        for verb in 0..100 {
            let mut opcodes = original_opcodes.clone();
            opcodes[1] = noun;
            opcodes[2] = verb;
            let output = run(opcodes)[0];
            if output == 19690720 {
                println!("{}", 100 * noun + verb);
            }
        }
    }


    Ok(())
}