use std::cmp::max;
use std::io;

fn fuel_formula(n: i32) -> i32 {
    max(0, n / 3 - 2)
}

fn fuel_for_module(module: i32) -> i32 {
    let mut sum = 0;
    let mut weight = module;
    loop {
        let recursive_fuel = fuel_formula(weight);
        if recursive_fuel > 0 {
            sum += recursive_fuel;
            weight = recursive_fuel;
        } else {
            break;
        }
    }
    sum
}

#[test]
fn test() {
    assert_eq!(fuel_for_module(1969), 966);
    assert_eq!(fuel_for_module(100756), 50346);
}

fn main() -> Result<(), std::io::Error> {
    let mut sum = 0;
    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input)?;
        let trimmed = input.trim().to_string();
        if trimmed.is_empty() {
            eprintln!("{}", sum);
            break Ok(());
        } else {
            let module = trimmed.parse::<i32>().unwrap();
            sum += fuel_for_module(module);
        }
    }
}
