use std::io::stdin;

fn count_valid_passwords(min: i32, max: i32) -> i32 {
    let mut counter = 0;
    for i in min..max {
        if is_valid(i) {
            counter += 1;
        }
    }
    counter
}

fn nth_digit(n: i32, num: i32) -> i32 {
    let mut temp = num;
    for i in 0..n {
        temp /= 10;
    }
    temp % 10
}

fn is_valid(pass: i32) -> bool {
    if pass < 100000 || pass >= 999999 { return false; };
    let mut dig = nth_digit(5, pass);
    let mut found = false;
    let mut streak = 1;
    let mut streak_dig = dig;
    for j in 1..6 {
        let i = 5 - j;
        let next_dig = nth_digit(i, pass);
        if next_dig < dig {
            return false;
        }
        dig = next_dig;

        if dig != streak_dig {
            if streak == 2 {
                found = true;
            }
            streak = 1;
        } else {
            streak += 1;
        }
        streak_dig = dig;
    }
    if streak == 2 {
        found = true;
    }
    found
}

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    let mut minmax = input.trim().split('-');
    let min = minmax.next().unwrap().parse::<i32>().unwrap();
    let max = minmax.next().unwrap().parse::<i32>().unwrap();
    println!("{}", count_valid_passwords(min, max));
}

#[test]
fn test() {
    assert!(is_valid(112233));
    assert!(!is_valid(123444));
    assert!(is_valid(111122));
}