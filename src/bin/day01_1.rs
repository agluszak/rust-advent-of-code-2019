use std::cmp::max;
use std::io;

fn fuel_formula(n: i32) -> i32 {
    max(0, n / 3 - 2)
}

fn main() -> Result<(), std::io::Error> {
    let mut sum = 0;
    loop {
        let mut input = String::new();
        io::stdin().read_line(&mut input)?;
        let trimmed = input.trim().to_string();
        if trimmed.is_empty() {
            eprintln!("{}", sum);
            break Ok(());
        } else {
            let number = trimmed.parse::<i32>().unwrap();
            sum += fuel_formula(number);
        }
    }
}
