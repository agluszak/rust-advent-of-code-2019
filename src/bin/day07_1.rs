use std::cmp::max;
use std::io;

use itertools::Itertools;

fn mode(opcode: i32, nth_param: usize) -> i32 {
    let mut rest = opcode / 100;
    for _ in 1..nth_param {
        rest /= 10;
    }
    rest % 10
}

fn value(opcodes: &Vec<i32>, position: usize, nth_param: usize) -> i32 {
    let opcode = opcodes[position];
    match mode(opcode, nth_param) {
        0 => opcodes[opcodes[position + nth_param] as usize],
        1 => opcodes[position + nth_param],
        _ => panic!("Incorrect mode")
    }
}

fn set_value(opcodes: &mut Vec<i32>, position: usize, nth_param: usize, value: i32) {
    let opcode = opcodes[position];
    match mode(opcode, nth_param) {
        0 => {
            let dest = opcodes[position + nth_param] as usize;
            opcodes[dest] = value
        }
        1 => opcodes[position + nth_param] = value,
        _ => panic!("Incorrect mode")
    }
}


fn run(mut opcodes: &mut Vec<i32>, mut input: Vec<i32>) -> i32 {
    let mut position = 0;
    loop {
        let opcode = opcodes[position];
        let operation = opcode % 100;
        match operation {
            1 => {
                let dest = opcodes[position + 3] as usize;
                opcodes[dest] = value(&opcodes, position, 1) + value(&opcodes, position, 2);
                position += 4;
            }
            2 => {
                let dest = opcodes[position + 3] as usize;
                opcodes[dest] = value(&opcodes, position, 1) * value(&opcodes, position, 2);
                position += 4;
            }
            3 => {
                let dest = opcodes[position + 1] as usize;
                opcodes[dest] = input.pop().unwrap();
                position += 2;
            }
            4 => {
                break value(&opcodes, position, 1);
                position += 2;
            }
            5 => {
                let check = value(&opcodes, position, 1);
                if check != 0 {
                    position = value(&opcodes, position, 2) as usize;
                } else {
                    position += 3;
                }
            }
            6 => {
                let check = value(&opcodes, position, 1);
                if check == 0 {
                    position = value(&opcodes, position, 2) as usize;
                } else {
                    position += 3;
                }
            }
            7 => {
                let first = value(&opcodes, position, 1);
                let second = value(&opcodes, position, 2);
                if first < second {
                    set_value(&mut opcodes, position, 3, 1);
                } else {
                    set_value(&mut opcodes, position, 3, 0);
                }
                position += 4;
            }
            8 => {
                let first = value(&opcodes, position, 1);
                let second = value(&opcodes, position, 2);
                if first == second {
                    set_value(&mut opcodes, position, 3, 1);
                } else {
                    set_value(&mut opcodes, position, 3, 0);
                }
                position += 4;
            }
            v => {
                panic!("Incorrect value {}", v)
            }
        }
    }
}

fn main() -> Result<(), std::io::Error> {
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    let split = input.trim().split(',');
    let original_opcodes = split.map(|x| x.parse::<i32>().unwrap()).collect::<Vec<_>>();

    let mut best = 0;
    let possible_phases = vec![0, 1, 2, 3, 4];
    for phases in possible_phases.iter().permutations(5) {
        let mut signal = 0;
        let mut program = original_opcodes.clone();
        for i in 0..5 {
            signal = run(&mut program, vec![signal, *phases[i]]);
        }
        best = max(best, signal);
    }

    println!("{}", best);
    Ok(())
}

