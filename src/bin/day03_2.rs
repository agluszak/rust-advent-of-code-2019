use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::io::stdin;
use std::iter::FromIterator;

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
struct Coord(i32, i32);

enum Dir { L, U, R, D }

struct Twist(Dir, i32);

fn parse_twist(s: String) -> Twist {
    let dir = match s.chars().next().unwrap() {
        'L' => Dir::L,
        'U' => Dir::U,
        'R' => Dir::R,
        'D' => Dir::D,
        _ => panic!("Wrong dir")
    };
    let len = s[1..].parse::<i32>().unwrap();
    Twist(dir, len)
}

fn append_coords(list: &mut Vec<(Coord, i32)>, twist: &Twist) {
    let (mut last, mut steps) = list[list.len() - 1];

    for _ in 0..twist.1 {
        last = match twist.0 {
            Dir::L => {
                Coord(last.0 - 1, last.1)
            }
            Dir::U => {
                Coord(last.0, last.1 + 1)
            }
            Dir::R => {
                Coord(last.0 + 1, last.1)
            }
            Dir::D => {
                Coord(last.0, last.1 - 1)
            }
        };
        steps += 1;
        list.push((last, steps));
    }
}

fn read() -> String {
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    input
}

fn read_path(s: String) -> HashMap<Coord, i32> {
    let raw_path: Vec<Twist> = s.trim().split(',').map(|s| parse_twist(s.to_owned())).collect::<Vec<_>>();
    let mut vec = vec![(Coord(0, 0), 0)];
    raw_path.iter().for_each(|t| append_coords(&mut vec, t));
    let mut result = HashMap::new();
    for (coord, steps) in vec {
        if result.get(&coord).map_or(true, |&old_steps| old_steps > steps) {
            result.insert(coord, steps);
        }
    }
    result.remove(&Coord(0, 0));
    result
}

fn keys_set<K, V>(map: &HashMap<K, V>) -> HashSet<K> where K: Hash + Eq + Clone {
    HashSet::from_iter(map.keys().cloned())
}

fn min_steps(s1: HashMap<Coord, i32>, s2: HashMap<Coord, i32>) -> i32 {
    keys_set(&s1)
        .intersection(&keys_set(&s2))
        .map(|c| s1.get(c).unwrap() + s2.get(c).unwrap())
        .min()
        .unwrap()
}

fn main() {
    let first_path: HashMap<Coord, i32> = read_path(read());
    let second_path: HashMap<Coord, i32> = read_path(read());
    println!("{}", min_steps(first_path, second_path));
}

#[test]
fn test() {
    assert_eq!(min_steps(read_path("R75,D30,R83,U83,L12,D49,R71,U7,L72".to_string()), read_path("U62,R66,U55,R34,D71,R55,D58,R83".to_string())), 610);
    assert_eq!(min_steps(read_path("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51".to_string()), read_path("U98,R91,D20,R16,D67,R40,U7,R15,U6,R7".to_string())), 410);
}
