use std::collections::{HashMap, HashSet, VecDeque};
use std::io;

use regex::Regex;

fn main() {
    let regex = Regex::new(r"(.+)\)(.+)").unwrap();
    let mut input = String::new();
    let mut neighbours: HashMap<String, Vec<String>> = HashMap::new();
    let mut distance: HashMap<String, i32> = HashMap::new();
    let mut visited: HashSet<String> = HashSet::new();
    loop {
        input.clear();
        io::stdin().read_line(&mut input).unwrap();
        if let Some(matches) = regex.captures(input.trim()) {
            let parent = matches[1].to_string();
            let child = matches[2].to_string();
            neighbours.entry(parent.clone())
                .and_modify(|v| v.push(child.clone()))
                .or_insert(vec![child.clone()]);
            neighbours.entry(child.clone())
                .and_modify(|v| v.push(parent.clone()))
                .or_insert(vec![parent.clone()]);
        } else {
            break;
        }
    }

    let initial = "YOU".to_string();
    let target = "SAN".to_string();
    let mut queue = VecDeque::new();
    queue.push_back(initial.clone());
    distance.insert(initial.clone(), 0);

    while let Some(v) = queue.pop_back() {
        visited.insert(v.clone());
        let &v_size = distance.get(&v).unwrap();
        for neighbour in neighbours.get(&v).unwrap_or(&Vec::new()) {
            if !visited.contains(neighbour) {
                distance.insert(neighbour.clone(), v_size + 1);
                queue.push_back(neighbour.clone());
            }
        }
    }
    println!("{}", distance.get(&target).unwrap() - 2);
}